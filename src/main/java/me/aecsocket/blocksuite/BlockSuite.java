package me.aecsocket.blocksuite;

import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelPipeline;
import me.aecsocket.blocksuite.proxy.ProxyDuplexHandler;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/** The main plugin class. */
public class BlockSuite extends JavaPlugin {
    private static BlockSuite instance;

    /** If the custom {@link ChannelDuplexHandler} has been injected or not. */
    private boolean injected;
    /** The custom {@link ChannelDuplexHandler} to inject into the channel. */
    private ChannelDuplexHandler channelDuplexHandler;

    public static BlockSuite getInstance() { return instance; }

    @Override
    public void onEnable() {
        instance = this;

        injected = false;
        channelDuplexHandler = new ProxyDuplexHandler();

        getServer().getPluginManager().registerEvents(new EventHandle(), this);
    }

    /** Gets if the custom {@link ChannelDuplexHandler} has been injected or not.
     * @return If the custom {@link ChannelDuplexHandler} has been injected or not.
     */
    public boolean isInjected() { return injected; }

    /** Injects the custom {@link ChannelDuplexHandler} into the channel. */
    public void inject() {
        if (injected)
            throw new IllegalStateException("Already injected");

        injected = true;
        getServer().getOnlinePlayers().forEach(this::inject);
    }

    /** Removes the custom {@link ChannelDuplexHandler} from the channel. */
    public void uninject() {
        if (!injected)
            throw new IllegalStateException("Not injected");

        injected = false;
        getServer().getOnlinePlayers().forEach(this::uninject);
    }

    /** Injects the custom {@link ChannelDuplexHandler} into the channel for a specific {@link Player}.
     * @param player The {@link Player}.
     */
    public void inject(Player player) {
        ChannelPipeline pipeline = ((CraftPlayer)player).getHandle().playerConnection.networkManager.channel.pipeline();
        pipeline.addBefore("packet_handler", player.getName(), channelDuplexHandler);
    }

    /** Removes the custom {@link ChannelDuplexHandler} from the channel for a specific {@link Player}.
     * @param player The {@link Player}.
     */
    public void uninject(Player player) {
        Channel channel = ((CraftPlayer)player).getHandle().playerConnection.networkManager.channel;
        channel.eventLoop().submit(() -> {
            channel.pipeline().remove(player.getName());
            return null;
        });
    }

    /** Gets the custom {@link ChannelDuplexHandler} to inject into the channel.
     * @return The custom {@link ChannelDuplexHandler} to inject into the channel.
     */
    public ChannelDuplexHandler getChannelDuplexHandler() { return channelDuplexHandler; }

    /** Sets the custom {@link ChannelDuplexHandler} to inject into the channel, and re-injects afterwards.
     * @param channelDuplexHandler The custom {@link ChannelDuplexHandler} to inject into the channel.
     */
    public void setChannelDuplexHandler(ChannelDuplexHandler channelDuplexHandler) {
        uninject();
        this.channelDuplexHandler = channelDuplexHandler;
        inject();
    }
}
