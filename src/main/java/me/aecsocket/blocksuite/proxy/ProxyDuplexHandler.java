package me.aecsocket.blocksuite.proxy;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import me.aecsocket.blocksuite.proxy.function.core.ProxyFunction;
import me.aecsocket.blocksuite.proxy.function.core.ProxyReadFunction;
import me.aecsocket.blocksuite.proxy.function.core.ProxyWriteFunction;
import me.aecsocket.blocksuite.proxy.function.event.ProxyReadEvent;
import me.aecsocket.blocksuite.proxy.function.event.ProxyWriteEvent;
import net.minecraft.server.v1_14_R1.Packet;

import java.util.ArrayList;
import java.util.List;

/** The custom {@link ChannelDuplexHandler}. */
@ChannelHandler.Sharable
public class ProxyDuplexHandler extends ChannelDuplexHandler {
    /** A {@link List} of {@link ProxyFunction} modules. */
    private List<ProxyFunction> functions;

    public ProxyDuplexHandler(List<ProxyFunction> functions) {
        this.functions = functions;
    }

    public ProxyDuplexHandler() {
        this(new ArrayList<>());
    }

    /** Gets the {@link List} of {@link ProxyFunction} modules.
     * @return The {@link List} of {@link ProxyFunction} modules.
     */
    public List<ProxyFunction> getFunctions() { return new ArrayList<>(functions); }

    /** Gets if the {@link List} of {@link ProxyFunction}s has a {@link ProxyFunction}.
     * @param function The {@link ProxyFunction}.
     * @return If it contains the {@link ProxyFunction} or not.
     */
    public boolean hasFunction(ProxyFunction function) { return functions.contains(function); }

    /** Registers a {@link ProxyFunction} into the {@link List} of {@link ProxyFunction}s.
     * @param function The {@link ProxyFunction} to register.
     */
    public void registerFunction(ProxyFunction function) { functions.add(function); }

    /** Registers a {@link ProxyFunction} into the {@link List} of {@link ProxyFunction}s if it has not been already registered.
     * @param function The {@link ProxyFunction} to register.
     */
    public void registerFunctionIfAbsent(ProxyFunction function) { if (!hasFunction(function)) registerFunction(function); }

    /** Unregisters a {@link ProxyFunction} from the {@link List} of {@link ProxyFunction}s.
     * @param function The {@link ProxyFunction} to unregister.
     */
    public void unregisterFunction(ProxyFunction function) { functions.remove(function); }

    @Override
    public void channelRead(ChannelHandlerContext context, Object packet) throws Exception {
        if (packet instanceof Packet<?>) {
            ProxyReadEvent event = new ProxyReadEvent(context, (Packet<?>)packet);
            for (ProxyFunction function : functions) {
                if (function instanceof ProxyReadFunction)
                    ((ProxyReadFunction)function).read(event);
            }

            if (event.isCancelled())
                return;
        }
        super.channelRead(context, packet);
    }

    @Override
    public void write(ChannelHandlerContext context, Object packet, ChannelPromise promise) throws Exception {
        if (packet instanceof Packet<?>) {
            ProxyWriteEvent event = new ProxyWriteEvent(context, (Packet<?>)packet, promise);
            for (ProxyFunction function : functions) {
                if (function instanceof ProxyWriteFunction)
                    ((ProxyWriteFunction)function).write(event);
            }

            if (event.isCancelled())
                return;
        }
        super.write(context, packet, promise);
    }
}
