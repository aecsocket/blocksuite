package me.aecsocket.blocksuite.proxy.function.event;

import io.netty.channel.ChannelHandlerContext;
import net.minecraft.server.v1_14_R1.Packet;

/** An event in which the proxy reads a packet. */
public class ProxyReadEvent extends ProxyPacketEvent {
    public ProxyReadEvent(boolean cancelled, ChannelHandlerContext context, Packet<?> packet) {
        super(cancelled, context, packet);
    }

    public ProxyReadEvent(ChannelHandlerContext context, Packet<?> packet) {
        super(context, packet);
    }
}
