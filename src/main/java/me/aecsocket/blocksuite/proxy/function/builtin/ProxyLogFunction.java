package me.aecsocket.blocksuite.proxy.function.builtin;

import me.aecsocket.blocksuite.BlockSuite;
import me.aecsocket.blocksuite.proxy.function.core.ProxyReadFunction;
import me.aecsocket.blocksuite.proxy.function.core.ProxyWriteFunction;
import me.aecsocket.blocksuite.proxy.function.event.ProxyEvent;
import me.aecsocket.blocksuite.proxy.function.event.ProxyPacketEvent;
import me.aecsocket.blocksuite.proxy.function.event.ProxyReadEvent;
import me.aecsocket.blocksuite.proxy.function.event.ProxyWriteEvent;
import me.aecsocket.pluginutils.util.Utils;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;

/** A module to log inbound and outbound packets to the console. */
public class ProxyLogFunction implements ProxyReadFunction, ProxyWriteFunction {
    /** A {@link List} of {@link Predicate}s which determine if a packet is logged or not. */
    private List<Predicate<ProxyPacketEvent>> predicates;
    /** The plugin {@link Logger}. */
    private Logger logger;

    public ProxyLogFunction(List<Predicate<ProxyPacketEvent>> predicates) {
        this.predicates = predicates;
        this.logger = BlockSuite.getInstance().getLogger();
    }

    public ProxyLogFunction() {
        this(new ArrayList<>());
    }

    /** Gets a {@link List} of {@link Predicate}s which determine if a packet is logged or not.
     * @return A {@link List} of {@link Predicate}s which determine if a packet is logged or not.
     */
    public List<Predicate<ProxyPacketEvent>> getPredicates() { return new ArrayList<>(predicates); }

    /** Adds a {@link Predicate} to the {@link List} of {@link Predicate}s.
     * @param predicate The {@link Predicate} to add.
     */
    public void addPredicate(Predicate<ProxyPacketEvent> predicate) { predicates.add(predicate); }

    /** Removes a {@link Predicate} from the {@link List} of {@link Predicate}s.
     * @param predicate The {@link Predicate} to remove.
     */
    public void removePredicate(Predicate<ProxyEvent> predicate) { predicates.remove(predicate); }

    private boolean test(ProxyPacketEvent event) {
        for (Predicate<ProxyPacketEvent> predicate : predicates) {
            if (!predicate.test(event))
                return false;
        }
        return true;
    }

    @Override
    public void read(ProxyReadEvent event) {
        if (test(event))
            logger.log(Level.INFO, String.format("%s< %s", ChatColor.GREEN, Utils.getToString(event.getPacket())));
    }

    @Override
    public void write(ProxyWriteEvent event) {
        if (test(event))
            logger.log(Level.INFO, String.format("%s> %s", ChatColor.BLUE, Utils.getToString(event.getPacket())));
    }
}
