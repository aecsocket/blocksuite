package me.aecsocket.blocksuite.proxy.function.core;

import me.aecsocket.blocksuite.proxy.function.event.ProxyReadEvent;

/** A module which runs on a {@link ProxyReadEvent}. */
public interface ProxyReadFunction extends ProxyFunction {
    /** The proxy read a packet.
     * @param event The {@link ProxyReadEvent}.
     */
    void read(ProxyReadEvent event);
}
