package me.aecsocket.blocksuite.proxy.function.event;

/** A generic event class. */
public class ProxyEvent {
    /** If the event has been cancelled or not. */
    private boolean cancelled;

    public ProxyEvent(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public ProxyEvent() {
        this(false);
    }

    /** Gets if the event has been cancelled or not.
     * @return If the event has been cancelled or not.
     */
    public boolean isCancelled() { return cancelled; }

    /** Sets if the event has been cancelled or not.
     * @param cancelled If the event has been cancelled or not.
     */
    public void setCancelled(boolean cancelled) { this.cancelled = cancelled; }
}
