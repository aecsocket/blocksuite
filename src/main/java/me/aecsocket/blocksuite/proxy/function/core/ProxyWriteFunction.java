package me.aecsocket.blocksuite.proxy.function.core;

import me.aecsocket.blocksuite.proxy.function.event.ProxyWriteEvent;

/** A module which runs on a {@link ProxyWriteEvent}. */
public interface ProxyWriteFunction extends ProxyFunction {
    /** The proxy wrote a packet.
     * @param event The {@link ProxyWriteEvent}.
     */
    void write(ProxyWriteEvent event);
}
