package me.aecsocket.blocksuite.proxy.function.event;

import io.netty.channel.ChannelHandlerContext;
import net.minecraft.server.v1_14_R1.Packet;

/** A generic proxy event with a {@link ChannelHandlerContext} and a {@link Packet}. */
public class ProxyPacketEvent extends ProxyEvent {
    /** The {@link ChannelHandlerContext}. */
    private ChannelHandlerContext context;
    /** The {@link Packet}. */
    private Packet<?> packet;

    public ProxyPacketEvent(boolean cancelled, ChannelHandlerContext context, Packet<?> packet) {
        super(cancelled);
        this.context = context;
        this.packet = packet;
    }

    public ProxyPacketEvent(ChannelHandlerContext context, Packet<?> packet) {
        this(false, context, packet);
    }

    /** Gets the {@link ChannelHandlerContext}.
     * @return The {@link ChannelHandlerContext}.
     */
    public ChannelHandlerContext getContext() { return context; }

    /** Sets the {@link ChannelHandlerContext}.
     * @param context The {@link ChannelHandlerContext}.
     */
    public void setContext(ChannelHandlerContext context) { this.context = context; }

    /** Gets the {@link Packet}.
     * @return The {@link Packet}.
     */
    public Packet<?> getPacket() { return packet; }

    /** Sets the {@link Packet}.
     * @param packet The {@link Packet}.
     */
    public void setPacket(Packet<?> packet) { this.packet = packet; }
}
