package me.aecsocket.blocksuite.proxy.function.event;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import net.minecraft.server.v1_14_R1.Packet;

/** An event in which the proxy writes a packet. */
public class ProxyWriteEvent extends ProxyPacketEvent {
    /** The {@link ChannelPromise}. */
    private ChannelPromise promise;

    public ProxyWriteEvent(boolean cancelled, ChannelHandlerContext context, Packet<?> packet, ChannelPromise promise) {
        super(cancelled, context, packet);
        this.promise = promise;
    }

    public ProxyWriteEvent(ChannelHandlerContext context, Packet<?> packet, ChannelPromise promise) {
        this(false, context, packet, promise);
    }

    /** Gets the {@link ChannelPromise}.
     * @return The {@link ChannelPromise}.
     */
    public ChannelPromise getPromise() { return promise; }

    /** Sets the {@link ChannelPromise}.
     * @param promise The {@link ChannelPromise}.
     */
    public void setPromise(ChannelPromise promise) { this.promise = promise; }
}
