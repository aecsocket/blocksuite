# BlockSuite

BurpSuite but for Minecraft: server packet listener

---

This plugin allows you to intercept all the network traffic between clients and servers, and run functions on them. It
has a modular system, allowing you to add and remove whichever functions you want to run. It was designed to be easy to
use for debugging and testing, however it can be applied to many different functions.

## For server owners

Put the JAR in `$SERVER/plugins` and it will work if any plugins require it.

## For developers

Javadoc is available at [src/main/javadoc](src/main/javadoc).

### ProxyDuplexHandler

A `ProxyDuplexHander` is a custom `ChannelDuplexHandler` which allows you to write modules for it, triggering on read
and write events.

### ProxyFunction

A `ProxyFunction` is a module you can put into a `ProxyDuplexHandler`. You should use either a `ProxyReadFunction` or
a `ProxyWriteFunction` (or both) to base your function off of.

#### ProxyLogFunction

There is an in-built function called `ProxyLogFunction`. This is a simple demonstration, and shows how you can use this
to log packets in a simple way, even allowing you to add predicates to determine what gets logged. 

### Injecting/Uninjecting

By default, BlockSuite does not inject the ChannelDuplexHandler into the Channel. You must do this yourself by running
`BlockSuite#inject()` after you have initialized everything (set your own ChannelDuplexHandler, added your functions,
etc.)

### Code

Here is an example piece of code:

`MyFunction.java`
```java
// package...
// import...

public class MyFunction implements ProxyReadFunction, ProxyWriteFunction {
    @Override
    public void read(ProxyReadEvent event) {
        if (event.getPacket() instanceof PacketPlayInBlockPlace) {
            System.out.println("Block tried to place");
        }
    }
    
    @Override
    public void write(ProxyWriteEvent event) {
        if (event.getPacket() instanceof PacketPlayOutAnimation) {
            event.setCancelled(true);
        }
    }
}
```

`MyPlugin.java`
```java
// package...
// import...

public class MyPlugin extends JavaPlugin {
    @Override
    public void onEnable() {
        BlockSuite api = BlockSuite.getInstance();
        ProxyDuplexHandler proxy = (ProxyDuplexHandler)api.getChannelDuplexHandler();
        proxy.registerFunction(new MyFunction());
 
        api.inject();
    }
    
    @Override
    public void onDisable() {
        BlockSuite api = BlockSuite.getInstance();
        api.uninject(); // REMEMBER TO UNINJECT!
    }
}
```

## Download

Currently there is no JAR download available. You must compile it yourself.